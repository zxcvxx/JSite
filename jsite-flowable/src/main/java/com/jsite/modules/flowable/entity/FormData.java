/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.entity;

import com.jsite.common.persistence.DataEntity;
import com.jsite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;

/**
 * 表单数据表生成Entity
 * @author liuruijun
 * @version 2019-04-01
 */
public class FormData extends DataEntity<FormData> {
	
	private static final long serialVersionUID = 1L;
	private String formId;		// 表单编号
	private String procInsId;		// 流程实例ID
	private String fieldName;		// 字段名称
	private String fieldValue;		// 字段值
	private String fieldLargeValue;		// 大文本字段值

	private String procDefKey;		// 流程定义KEY 表单数据暂存功能使用

	public FormData() {
		super();
	}

	public FormData(String formId, String procInsId, String fieldName) {
		super();
		this.formId = formId;
		this.procInsId = procInsId;
		this.fieldName = fieldName;
	}

	public FormData(String formId, String procInsId) {
        this.formId = formId;
        this.procInsId = procInsId;
    }

    public FormData(String formId, String procDefKey, User createBy) {
        this.formId = formId;
        this.procDefKey = procDefKey;
        this.createBy = createBy;
    }

	@Length(min=1, max=64, message="表单编号长度必须介于 1 和 64 之间")
	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	@Length(min=0, max=255, message="流程实例ID长度必须介于 0 和 255 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	
	@Length(min=0, max=64, message="字段名称长度必须介于 0 和 64 之间")
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Length(min=0, max=255, message="字段值长度必须介于 0 和 255 之间")
	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	
	@Length(min=0, max=3000, message="大文本字段值长度必须介于 0 和 3000 之间")
	public String getFieldLargeValue() {
		return fieldLargeValue;
	}

	public void setFieldLargeValue(String fieldLargeValue) {
		this.fieldLargeValue = fieldLargeValue;
	}

	public String getProcDefKey() {
		return procDefKey;
	}

	public void setProcDefKey(String procDefKey) {
		this.procDefKey = procDefKey;
	}
}