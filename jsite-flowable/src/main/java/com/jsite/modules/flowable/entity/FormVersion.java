package com.jsite.modules.flowable.entity;

import com.jsite.common.persistence.DataEntity;

public class FormVersion extends DataEntity<FormVersion> {

    private String procInsId;
    private Integer modelVersion;

    public FormVersion() {}

    public FormVersion(String procInsId, String modelVersion) {
        this.procInsId = procInsId;
        this.modelVersion = Integer.parseInt(modelVersion);
    }

    public String getProcInsId() {
        return procInsId;
    }

    public void setProcInsId(String procInsId) {
        this.procInsId = procInsId;
    }

    public Integer getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(Integer modelVersion) {
        this.modelVersion = modelVersion;
    }
}
